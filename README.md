# openCRAN [![Build Status](https://gitlab.com/jangorecki/openCRAN/badges/master/build.svg)](https://gitlab.com/jangorecki/openCRAN/pipelines)

Main goal of openCRAN is to ease R packages delivery by serving build, test and release process for in-house deployment.  
Project originally grown from a script to process massive reverse dependency check against 150+ R packages in fresh isolated environments and in parallel.  

Consumer interacts with locally hosted nginx to install R packages, read documentation or check tests results.  
Developer interacts with locally hosted instance of [GitLab](https://gitlab.com) on [gitlab.local](http://gitlab.local). 
Push and store code, then GitLab orchestrate cross platform build, test and release process. Package sources and binaries for windows/macosx are in proper place allowing easy CRAN-like installation using `repos="http://cran.opencran.local"`. CRAN repo environments are `staging.opencran.local` on git commit and `cran.opencran.local` on git tag.  

openCRAN is an R package that provide wrappers to work with openCRAN environment. Package is useful in your workflow to gather Rcheck and src/bin from different jobs and render CRAN-like repo. Functions to provides dynamic templates for CI yaml file useful in initial setup. For setup and maintenance related functions you need openCRAN's suggested dependencies (git2r, curl, jsonlite). Package is not being used during `test` step, so has no impact on tests environment.  

openCRAN example setup consists GitLab and GitLab Runner services.  

## Deploy

Docker is being used in examples to improve ad-hoc reproducibility, you can deploy services on bare metal or vm.  

Setup openCRAN environment.  

```sh
git clone https://gitlab.com/jangorecki/openCRAN.git
cd openCRAN

# create network for our openCRAN environment
docker network create --subnet=183.141.1.0/24 opencran

# GitLab, adjust root password if required
sudo docker run -d --name gitlab \
    --net=opencran \
    --hostname gitlab.local \
    --ip 183.141.1.2 \
    --env 'GITLAB_ROOT_PASSWORD=rootroot' \
    --env 'GITLAB_HOST=http://gitlab.local' \
    --env GITLAB_OMNIBUS_CONFIG="nginx['custom_nginx_config'] = 'include /etc/nginx/conf.d/*.conf;'" \
    --restart always \
    --volume /srv/gitlab/config:/etc/gitlab \
    --volume /srv/gitlab/logs:/var/log/gitlab \
    --volume /srv/gitlab/data:/var/opt/gitlab \
    --volume $(pwd)/inst/nginx/opencran.conf:/etc/nginx/conf.d/opencran.conf \
    --volume /srv/opencran:/opencran \
    gitlab/gitlab-ce:latest
sudo sh -c "echo '183.141.1.2 opencran.local staging.opencran.local cran.opencran.local gitlab.local' >> /etc/hosts"

# after few minutes you can test this step opening local GitLab
xdg-open http://gitlab.local
```

Setup linux CI runner.  

```sh
docker run -d --name gitlab-runner \
    --net=opencran \
    --restart always \
    --hostname runner-linux.local \
    --ip 183.141.1.3 \
    --add-host "gitlab.local:183.141.1.2" \
    --volume /srv/gitlab-runner/config:/etc/gitlab-runner \
    --volume /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner:latest

# register CI runner to GitLab instance
# get TOKEN from web ui
xdg-open http://gitlab.local/admin/runners
docker exec gitlab-runner gitlab-runner register \
    --non-interactive \
    --registration-token "ws9zkv46-sHManopkfPC" \
    --url "http://gitlab.local/ci" \
    --tag-list "linux" \
    --description "CI runner linux builds" \
    --executor "docker" \
    --docker-network-mode opencran \
    --docker-extra-hosts "gitlab.local:183.141.1.2" \
    --docker-image docker.io/jangorecki/r-builder \
    --docker-volumes /srv/opencran/staging:/staging \
    --docker-volumes /srv/opencran/cran:/cran
```

Optionally setup CI runners for [Windows](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/windows.md) and [MacOSX](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/osx.md). Basic CI template for windows is provided.    
In case of Windows XP the following steps were required:
- install gitlab runner
- register runner, use `windows` tag for runner
- start gitlab-runner
- add `R`, `git` and `zip` to `PATH`
- restart

## Setup

openCRAN services are ready to process your delivery pipeline, but doesn't have any projects defined. You can create projects using GitLab web UI on [gitlab.local](http://gitlab.local) or straight from R with helper from openCRAN R package (requires suggested dependencies: [git2r](https://github.com/ropensci/git2r), [curl](https://github.com/jeroenooms/curl), [jsonlite](https://github.com/jeroenooms/jsonlite)) using `gl.projs.create` function. Aside from creating project in GitLab you need to supply your project with yaml file that defines CI pipeline. This can be produced from template using `pkgs.ci.add` function. Last step is to simply push your project to GitLab instance so it will start release workflow. This can be done from any git client or with `pkgs.push` function. `pkgs.push` will additionally initialize git repo if there isn't already on provided path, create project on GitLab, add default CI template, add `gl` remote if not present and then push to local GitLab.  

Following steps are the pre-requiresites for release pipeline:
- create project in your GitLab instance
- commit CI yaml to your project
- push project to GitLab

### Installation

```r
install.packages(
    "openCRAN",
    dependencies = TRUE, # will install suggested dependencies
    repos = paste0("https://", c("jangorecki.gitlab.io/openCRAN","cran.rstudio.com"))
)
```

## Usage

### Release: one-liner

Assuming you have access on [gitlab.local](http://gitlab.local) using `root/rootroot` access, alternatively use `gitlab.user`/`gitlab.pass` R options to customize.  

```r
openCRAN::pkgs.push(g<-git2r::clone("https://github.com/eddelbuettel/drat.git", file.path("/tmp","drat")))
browseURL("http://gitlab.local/root/drat/pipelines")
```

Once pipelines will finish you can consume your released package/docs as described in _Consume_ section

Below command will overwrite the template with CI to check various linux configurations and single windows build:  
- `linux-release` tests recent stable R version
- `linux-rdevel` tests development version of R
- `linux-vanilla` tests recent stable R version in minimal configuration, no suggested packages are being installed
- `windows-release` tests recent stable R version on windows

Run it only if you configured windows CI runner, otherwise you can just remove `windows-release` from `jobs` argument:    

```
openCRAN::pkgs.ci.add(g, jobs=c(paste("linux",c("release","vanilla","rdevel"),sep="-"),"windows-release"), overwrite=TRUE, commit=TRUE)
openCRAN::pkgs.push(g)
```

### Release: explained

Release multiple R packages from various sources in one batch.  

```r
stopifnot(sapply(c("git2r","jsonlite","curl"), requireNamespace, quietly=TRUE)) # suggested deps used
# clone from remote git repo
up.git = c(
    git2r="https://github.com/ropensci/git2r.git",
    jsonlite="https://github.com/jeroenooms/jsonlite.git"
)
unlink(file.path("/tmp", names(up.git)), recursive=TRUE, force=TRUE)
g = sapply(names(up.git), function(pkg) git2r::workdir(git2r::clone(up.git[[pkg]], file.path("/tmp", pkg))))

# add non-git packages straight from CRAN(-like) repos, will get clean git tree
up.cran = c(
    "h2o"="https://cran.rstudio.com",
    "bit64"="https://cran.rstudio.com",
    "data.table"="https://Rdatatable.github.io/data.table"
)
unlink(file.path("/tmp", names(up.cran)), recursive=TRUE, force=TRUE)
dl.pkgs = mapply(download.packages, pkgs=names(up.cran), repos=up.cran, destdir="/tmp")[2,]
# untar overwrite
unlink(file.path(dirname(dl.pkgs), names(dl.pkgs)), recursive=TRUE)
cran = mapply(function(pkg, file) {
                  untar(tarfile=file, exdir=dirname(file))
                  file.path(dirname(file), pkg)
              }, names(dl.pkgs), dl.pkgs)

# combine pkgs from git repos and cran
g = c(g, cran)

library(openCRAN)

# add ci
pkgs.ci.add(g, overwrite=TRUE)
# windows builds are by default disabled (prefixed with .), start runner on windows and register to gitlab, then remove leading dot from job name

# h2o needs java installed, overwrite default template, add another job for `vanilla`, view yaml using: cat(readLines(file.path(g[["h2o"]], ".gitlab-ci.yml")), sep="\n")
pkgs.ci.add(g[["h2o"]], overwrite=TRUE, jobs=paste("linux",c("release","vanilla"),sep="-"), image="jangorecki/r-builder-openjdk8")

# for data.table use even more jobs
pkgs.ci.add(g[["data.table"]], overwrite=TRUE, jobs=paste("linux",c("release","vanilla","rdevel"),sep="-"))

# push to gitlab on `gl` remote
pkgs.push(g)

# preview builds
browseURL("http://gitlab.local/root/h2o/pipelines")
```

### Release: from shell

Another example using shell rather than R.  
Extract `h2oEnsemble` from h2o-3 tree into own tree. Be aware `h2oEnsemble` unit tests are computing intensive.  
Add CI yaml, remote and push.  

```sh
mkdir h2o-tmp
cd h2o-tmp
git init h2o-3
cd h2o-3
git remote add origin https://github.com/h2oai/h2o-3.git
git config core.sparsecheckout true
echo "h2o-r/ensemble/h2oEnsemble-package/*" >> .git/info/sparse-checkout
git pull --depth=1 origin master
mv h2o-r/ensemble/h2oEnsemble-package ../h2oEnsemble
# extracted h2oEnsemble package
cd ../h2oEnsemble
git init
git add -A
git commit -m 'init snapshot of h2oEnsemble'
# create h2oEnsemble project on local gitlab
Rscript -e 'openCRAN::gl.projs.create("h2oEnsemble")'
# use r-builder-openjdk8 docker image
Rscript -e 'openCRAN::pkg.ci.add(".", commit=TRUE, image="jangorecki/r-builder-openjdk8")'
git remote add gl http://gitlab.local/root/h2oEnsemble.git
git push gl master # root/rootroot
xdg-open http://gitlab.local/root/h2oEnsemble/pipelines
```

It makes perfect sense to write extra docker layers for dependencies to ship in build environment, `r-builder-openjdk8` just adds `openjdk8` to `r-builder` image. Also R packages can be frozen in docker image/layer, but it is wise to test recent stable releases to detect breaking changes on time.  

## Consume

By default packages gets released on each commit to `staging.opencran.local` and on each git tag to `cran.opencran.local`. So now you can install package with standard R command.  

```r
install.packages("drat", repos="http://staging.opencran.local")
```

It can automatically ship binaries for built on windows/macosx and will work with `update.packages` etc.  

Packages gets their websites rendered similarly as on CRAN into [staging.opencran.local/web/packages/drat/](http://staging.opencran.local/web/packages/drat/). You can navigate over complete repo dir structure on [staging.opencran.local](http://staging.opencran.local).  
For readers of this readme I mirrored the end product of above examples at [jangorecki.github.io/openCRAN](https://jangorecki.github.io/openCRAN). Unlike built-in GitLab nginx server the gh-pages won't let you to browse dir structure, you can see it in [openCRAN@gh-pages](https://github.com/jangorecki/openCRAN/tree/gh-pages).  

## diff CRAN

1. openCRAN is for in-house delivery of mixed CRAN and non-CRAN packages  
2. openCRAN uses `git push` in place of CRAN's *submission*
3. openCRAN has two environments: `staging` (every push) and `cran` (only tags)
4. openCRAN render documentation to html and pdf, CRAN to pdf
5. by default openCRAN run checks on fewer platforms
6. openCRAN repository index `PACKAGES` file can ship extra metadata
7. CRAN is *official* and is running for years, openCRAN is just CRAN skeleton/orchestration
8. openCRAN can build and push docker images to docker repository index during release process

## Gotchas

- Lots of small things not yet implemented, only core functionality works, which is cross platform build and test, publish CRAN-line repo, publish docs, checks results.  
- If you setup runners to work concurrently be sure to not use concurrent runners for deployments, as multiple deployments to same environment should not happen at the same time.  
- Currently relaese does not depend if `R CMD check` results into notes, warnings or errors.  
- There is a built-in default limit of projects set to 10, you can change it on [gitlab.local/admin/users/root/edit](http://gitlab.local/admin/users/root/edit).  
- Any branch pushed will be deployed, edit CI yaml to restrict deploy to `master` branch if needed.  

## Cleaning up

Just to make readme complete below are the commands to cleanup your environment after running examples.  

Cleanup R packages from the working directories in `/tmp`, call from R:
```r
unlink(file.path("/tmp", c("drat", names(g))), TRUE, TRUE)
```

Purge everything that has been created, including GitLab instance, all data stored in your GitLab, and your CRAN-like repo dir, call from shell:
```sh
docker stop gitlab gitlab-runner
docker rm gitlab gitlab-runner
docker network rm opencran
sudo rm -r /srv/gitlab /srv/gitlab-runner /srv/opencran
```

Finally, edit your `/etc/hosts/` to remove ip redirection of `opencran.local` domain to `183.141.1.2`, call from shell:
```sh
sudo vim /etc/hosts
```
